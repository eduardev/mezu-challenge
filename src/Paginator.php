<?php
/**
 * Created by PhpStorm.
 * User: eduardev
 * Date: 08-04-2018
 * Time: 12:42
 */

namespace Eduardev\MezuChallenge;

class Paginator
{
    /**
     * @param int   $current_page   The current page number
     * @param int   $total_pages    Total available pages
     * @param int   $boundaries     Number of pages in the start and end
     * @param int   $around         Number of pages before and after the actual page
     *
     * @throws PaginatorException
     */
    public function get(int $current_page, int $total_pages, int $boundaries, int $around)
    {
        // Fail fast for total pages
        if ($total_pages < 1) {
            throw new PaginatorException('Total pages must be 1 or more');
        }

        // Fail fast for current page number
        if ($current_page < 1 || $current_page > $total_pages) {
            throw new PaginatorException(sprintf('Current Page must be between %d and %d', 1, $total_pages));
        }

        // Also fail fast here for boundaries: they must 1 or greater (personal assumption here)
        if ($boundaries < 1) {
            throw new PaginatorException('Boundaries must be 1 or greater');
        }

        // Start working, set some variables
        $output = '';           // This will hold our output string
        $i = 1;                 // The current page being iterated

        while ($i <= $total_pages) {
            // Check the boundaries
            if ($i <= $boundaries || $i > $total_pages - $boundaries) {
                $output .= "$i ";
                $i++;
                continue;
            }

            // Check for around (THIS includes current page)
            if ($i >= $current_page - $around && $i <= $current_page + $around) {
                $output .= "$i ";
                $i++;
                continue;
            }

            // If still here, we don't want to show pages
            $output .= '... ';

            // Now if we shown ... how much should we jump (to avoid a gazillion iterations) ?
            if ($i < $current_page) {
                // We need to jump to just before ($around) current page number
                $i = $current_page - $around;
            } else {
                // We need to jump just before ($boundaries) the end
                $i = ($total_pages - $boundaries) + 1;
            }
        }

        echo trim($output);
    }
}