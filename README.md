#Mezu Employment Challenge
Simple project containing the challenge exercise result for the proposed challenge. 

##Details
- Developed using PHP 7.2 (should be compatible with ^7.1)
- Project developed as a composer package
- PHPUnit included in the project as a dev-requirement within composer
- Unit tests on the `tests` folder

##Running
- Clone this project: `git clone git@bitbucket.org:eduardev/mezu-challenge.git`
- Run `composer install` (to install phpunit and create autoloader)
- Point browser to the `index.php` file in the root of the clone directory.