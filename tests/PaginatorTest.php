<?php
/**
 * Created by PhpStorm.
 * User: eduardev
 * Date: 08-04-2018
 * Time: 13:28
 */

use Eduardev\MezuChallenge;
use PHPUnit\Framework\TestCase;

class PaginatorTest extends TestCase
{

    /**
     * @var MezuChallenge\Paginator
     */
    protected $Paginator;

    public function setUp()
    {
        $this->Paginator = new MezuChallenge\Paginator();
    }

    public function testGetExceptionTotalPagesTooLow()
    {
        $this->expectException(MezuChallenge\PaginatorException::class);
        $this->Paginator->get(0, 0, 1, 0);
    }

    public function testGetExceptionCurrPageTooLow()
    {
        $this->expectException(MezuChallenge\PaginatorException::class);
        $this->Paginator->get(0, 2, 1, 0);
    }

    public function testGetExceptionCurrPageTooHigh()
    {
        $this->expectException(MezuChallenge\PaginatorException::class);
        $this->Paginator->get(3, 2, 1, 0);
    }

    public function testGetExceptionBoundariesTooLow()
    {
        $this->expectException(MezuChallenge\PaginatorException::class);
        $this->Paginator->get(1, 2, 0, 0);
    }

    public function testGetExceptionParam1TypeErrorString()
    {
        $this->expectException(TypeError::class);
        $this->Paginator->get("string", 1, 0, 0);
    }

    public function testGetExceptionParam2TypeErrorString()
    {
        $this->expectException(TypeError::class);
        $this->Paginator->get(1, "string", 0, 0);
    }

    public function testGetExceptionParam3TypeErrorString()
    {
        $this->expectException(TypeError::class);
        $this->Paginator->get(1, 1, "string", 0);
    }

    public function testGetExceptionParam4TypeErrorString()
    {
        $this->expectException(TypeError::class);
        $this->Paginator->get(1, 1, 0, "string");
    }

    public function testGetExample1()
    {
        $this->expectOutputString("1 ... 4 5");
        $this->Paginator->get(4, 5, 1, 0);
    }

    public function testGetExample2()
    {
        $this->expectOutputString("1 2 3 4 5 6 ... 9 10");
        $this->Paginator->get(4, 10, 2, 2);
    }

    public function testGet1()
    {
        $this->expectOutputString("1");
        $this->Paginator->get(1, 1, 1, 1);
    }

    public function testGet2()
    {
        $this->expectOutputString("1 2 ... 33 34 35 36 37 ... 99999998 99999999");
        $this->Paginator->get(35, 99999999, 2, 2);
    }

    public function testGet3()
    {
        $this->expectOutputString("1 2 3 4 5 6 7 8 9 10 11 12 13 14 ... 71 72 73 74 75 76 77 78 79 80");
        $this->Paginator->get(4, 80, 10, 10);
    }

    public function testGet4()
    {
        $this->expectOutputString("1 2 3 4 5 6 7 8 9");
        $this->Paginator->get(5, 9, 2, 2);
    }

    public function testGet5()
    {
        $this->expectOutputString("1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21");
        $this->Paginator->get(11, 21, 5, 5);
    }

    public function testGet6()
    {
        $this->expectOutputString("1 ... 100");
        $this->Paginator->get(1, 100, 1, 0);
    }
}