<?php
/**
 * Created by PhpStorm.
 * User: eduardev
 * Date: 08-04-2018
 * Time: 12:49
 */

// Autoload our application
require_once "vendor/autoload.php";

// Instantiate our Paginator
$Paginate = new Eduardev\MezuChallenge\Paginator();

if (!isset($_GET['cp'], $_GET['tp'], $_GET['b'], $_GET['a'])) {
    ?>
    <h3>You need to pass the following GET variables</h3>
    <pre>
        cp : For the current page number
        tp : For the total number of pages
        b  : For the boundaries values
        a  : For the around value
    </pre>
    <?php
    exit;
}

try {
    $Paginate->get($_GET['cp'], $_GET['tp'], $_GET['b'], $_GET['a']);
} catch (TypeError | \Eduardev\MezuChallenge\PaginatorException $exception) {
    echo '<h3>There was a problem with the pagination:</h3>';
    echo $exception->getMessage();
}